/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package inclinometer;

/**
 *
 * @author PedroRodriguez
 */
public class Calculation
{
    private double trueAngle;
    private double readAngle;
    private double height;
    private double length;
    private double difference;
    public Calculation()
    {
        trueAngle = 0;
        readAngle = 0;
        height = 0;
        length = 0;
    }
    public Calculation(double aReadAngle, double aHeight, double aLength)
    {
        readAngle = toRadian(aReadAngle);
        height = aHeight;
        length = aLength;
    }
    public double calculateTrueAngle()
    {
        trueAngle = Math.atan(length * Math.tan(readAngle) / (length - height));
        return trueAngle;
    }
    public double calculateDifference()
    {
        difference = (trueAngle - readAngle) / trueAngle;
        return difference;
    }
    public double toRadian(double angle)
    {
        angle = angle * 3.14 / 180;
        return angle;
    }
    public double toDegrees(double angle)
    {
        angle = angle * 180 / 3.14;
        return angle;
    }
    public double getReadAngle()
    {
        return readAngle;
    }
    public double getTrueAngle()
    {
        return trueAngle;
    }
    public double getHeight()
    {
        return height;
    }
    public double getLength()
    {
        return length;
    }
    public double round(double x)
    {
        x = x * 100;
        x = (int) x;
        x = (double) x;
        x = x / 100;
        return x;
    }
}
